isPalindrome :: Eq a => [a] -> Bool
isPalindrome list = (take (floorHalfLength) list) == (reverse $ drop (ceilingHalfLength) list)
    where floorHalfLength = (length list) `div` 2
          ceilingHalfLength = ((length list) + 1) `div` 2

import Data.Set
import LinkedList

removeDuplicates :: Ord a => List a -> Set a -> List a
removeDuplicates Empty _ = Empty
removeDuplicates list seen
    | member (listHead list) seen = removeDuplicates (listTail list) (insert (listHead list) seen)
    | otherwise = Cons (listHead list) (removeDuplicates (listTail list) (insert (listHead list) seen))

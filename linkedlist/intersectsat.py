from linkedlist import ListNode


def intersects_at(list1, list2):
    references = {id(o) for o in list1}
    for o in list2:
        if id(o) in references:
            return o
    else:
        return None


l1 = ListNode.fromlist([1,2,3,4,5])
l2 = ListNode.fromlist([1,2,3,4,5])
l3 = ListNode.fromlist([1,2,3,4])

l3.next_node.next_node.next_node.next_node = l1.next_node.next_node.next_node

print(intersects_at(l1, l3))
print(intersects_at(l1, l2))
print(intersects_at(l2, l3))

module LinkedList where

data List a = Cons a (List a) | Empty deriving (Eq, Show)

listHead :: List a -> a
listHead (Cons a _) = a

listTail :: List a -> List a
listTail Empty = Empty
listTail (Cons _ list) = list

listDrop :: Int -> List a -> List a
listDrop 0 list = list
listDrop k list = listDrop (k - 1) (listTail list)

listLength :: List a -> Int
listLength Empty = 0
listLength list = 1 + listLength (listTail list)

listFromArray :: Ord a => [a] -> List a
listFromArray [val] = Cons val Empty
listFromArray array = Cons (head array) (listFromArray $ tail array)

listFilter :: (a -> Bool) -> List a -> List a
listFilter _ Empty = Empty
listFilter func (Cons v tail) =
    if func v
        then Cons v $ listFilter func tail
        else listFilter func tail

listCat :: List a -> List a -> List a
listCat Empty second = second
listCat (Cons val Empty) second = Cons val second
listCat (Cons val list) second = Cons val $ listCat list second

listLeftPad :: List a -> a -> Int -> List a
listLeftPad list _ 0 = list
listLeftPad list value count = Cons value $ listLeftPad list value (count - 1)

listSum :: Integral a => List a -> List a -> a -> List a
listSum Empty Empty carry
    | carry == 0 = Empty
    | otherwise = Cons carry Empty
listSum first Empty carry = 
    Cons ((firstVal + carry) `mod` 10)
        (listSum (listTail first) Empty ((firstVal + carry) `div` 10))
    where firstVal = listHead first
listSum Empty second carry =
    Cons ((secondVal + carry) `mod` 10)
        (listSum Empty (listTail second) ((secondVal + carry) `div` 10))
    where secondVal = listHead second
listSum first second carry =
    Cons ((firstVal + secondVal + carry) `mod` 10)
        (listSum (listTail first) (listTail second) ((firstVal + secondVal + carry) `div` 10))
    where firstVal = listHead first
          secondVal = listHead second

listSumForward :: Integral a => List a -> List a -> a -> List a
listSumForward Empty Empty carry
    | carry == 0 = Empty
    | otherwise = Cons carry Empty
listSumForward first second carry
    | firstLength > secondLength = listSumForward first (listLeftPad second 0 (firstLength - secondLength)) carry
    | secondLength > firstLength = listSumForward (listLeftPad first 0 (secondLength - firstLength)) second carry 
    | otherwise = Cons ((sumVal `div` 10) + carry) $ listSumForward (listTail first) (listTail second) (sumVal `mod` 10)
    where firstLength = listLength first
          secondLength = listLength second
          sumVal = (listHead first) + (listHead second)

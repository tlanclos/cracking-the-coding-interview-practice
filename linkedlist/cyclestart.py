from linkedlist import ListNode

def cycle_start_at(l):
    p1 = l.next_node
    p2 = l.next_node.next_node
    while p1 is not p2:
        p1 = p1.next_node
        p2 = p2.next_node.next_node

    p2 = l
    while p1 is not p2:
        p1 = p1.next_node
        p2 = p2.next_node

    return p1


cyclepart1 = ListNode.fromlist([1, 2, 3])
cyclepart2 = ListNode.fromlist([4, 5, 6])
cyclepart1.next_node.next_node.next_node = cyclepart2
cyclepart2.next_node.next_node.next_node = cyclepart1.next_node.next_node

print(cycle_start_at(cyclepart1))

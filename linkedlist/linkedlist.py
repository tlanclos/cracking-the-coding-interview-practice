class ListNode:
    maxrepr = 10

    def __init__(self, val, next_node=None):
        self.val = val
        self.next_node = next_node

    def __iter__(self):
        node = self
        while node:
            yield node
            node = node.next_node

    @staticmethod
    def fromlist(list):
        if not list:
            return None
        else:
            return ListNode(list[0], next_node=ListNode.fromlist(list[1:]))

    def __repr__(self):
        def _generate_values():
            listit = iter(self)
            _maxrepr = self.maxrepr
            while True:
                v = next(listit)
                if v is None:
                    break
                elif _maxrepr <= 0:
                    yield '...'
                    break
                else:
                    _maxrepr -= 1
                    yield str(v.val)

        return ' -> '.join(_generate_values())

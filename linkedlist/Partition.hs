import LinkedList

partition :: Ord a => List a -> a -> List a
partition list pivot = listCat (listFilter (\x -> x < pivot) list) (listFilter (\x -> x >= pivot) list)

from linkedlist import ListNode


def delete_middle_node(node):
    while node.next_node.next_node is not None:
        node.val = node.next_node.val
        node = node.next_node
    node.val = node.next_node.val
    node.next_node = None


def delete_middle_node2(node):
    if node.next_node.next_node is None:
        node.val = node.next_node.val
        node.next_node = None
    else:
        node.val = node.next_node.val
        delete_middle_node2(node.next_node)

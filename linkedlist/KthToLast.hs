import LinkedList

kthToLast :: Ord a => List a -> Int -> a
kthToLast list k = listHead $ listDrop ((listLength list) - k - 1) list
